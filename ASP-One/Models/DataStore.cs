﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_One.Models
{
    public static class DataStore
    {
        public static IList<QuotesViewModel> DataAccess { get; set; } = new List<QuotesViewModel>
        {
            new QuotesViewModel
            {
                Quote = "At all at all, na him bad pass",
                Author = "Anonymous"
            },
            new QuotesViewModel
            {
                Quote = "NullReference Exception",
                Author = "C#"
            },
            new QuotesViewModel
            {
                Quote = "Innit",
                Author = "GrandMaster KC"
            },
            new QuotesViewModel
            {
                Quote = "There's only one thing to do",
                Author = "SageTheUruly"
            }
        };

        public static QuotesViewModel GetRandomQuote()
        {
            var quote2Return = DataAccess[new Random().Next(DataAccess.Count)];
            return quote2Return;
        }
    }
}
