﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_One.Models
{
    public class QuotesViewModel
    {
        [Required]
        public string Quote { get; set; }

        [Required]
        public string Author { get; set; }


    }
}
