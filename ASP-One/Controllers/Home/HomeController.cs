﻿using Microsoft.AspNetCore.Mvc;
using ASP_One.Models;

namespace ASP_One.Controllers.Home
{
    public class HomeController: Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult Register()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Register(QuotesViewModel qoutesVM)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            DataStore.DataAccess.Add(new QuotesViewModel
            {
                Quote = qoutesVM.Quote,
                Author = qoutesVM.Author
            });

            return View("Index", DataStore.GetRandomQuote());
        }

    }
}
